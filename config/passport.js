const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const User = require('../models/user');
const config = require('../config/database');

module.exports = (passport) => {
    let opts = {};
    // opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
    opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme('JWT');
    opts.secretOrKey = config.secret;
    console.log("OPTS : ", opts)
    passport.use(new JwtStrategy(opts, (jwt_payload, done) => {
        console.log("payload ", jwt_payload);
        // User.findOne({id: jwt_payload.sub}, (err, user) => {
        User.getUserById(jwt_payload.user._id, (err, user) => {
            if(err) {
                return done(err, false);
            }

            if(user) {
                return done(null, user);
            } else {
                return done(null, false);
            }
        })
    }))
}
