const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');
var Schema = mongoose.Schema;

//User Schema
const UserSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    firstname: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    winNumber: {
        type: Number,
        default: 0
    },
    looseNumber: {
        type: Number,
        default: 0
    },
    played: {
        type: Boolean,
        default: false
    },
    partInProgress: {
        type: Boolean,
        default: false
    },
    game: {
        type: Schema.Types.ObjectId, ref: 'Game',
        default: null
    }
});

const User = module.exports = mongoose.model('User', UserSchema);

module.exports.getUserById = (id, callback)=>{
    User.findById(id, callback);
}

module.exports.getUserByEmail = (email, callback)=>{
    const query = {email: email}
    User.findOne(query, callback);
}

module.exports.addUser = (newUser, callback)=>{
    bcrypt.genSalt(10, (err, salt)=> {
        bcrypt.hash(newUser.password, salt, (err, hash) => {
            if(err) throw err;
            newUser.password = hash;
            newUser.save(callback)
        })
    })
}

module.exports.comparePassword = (candidatePassword, hash, callback) => {
    bcrypt.compare(candidatePassword, hash, (err, isMatch) => {
        if(err) throw err;
        callback(null, isMatch);
    })
}
