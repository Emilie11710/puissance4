const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
const cors = require("cors");
const passport = require("passport");
const mongoose = require("mongoose");

const config = require("./config/database");

// Connect to database
mongoose.connect(config.database, {
  useUnifiedTopology: true,
  useNewUrlParser: true,
});
// When connected
mongoose.connection.on("connected", () => {
  // console.log(`Connected to database ${config.database}`);
  console.log(`Connected to database to remote MOngoDB puissance4 database`);

});
// NOt connected
mongoose.connection.on("error", (err) => {
  console.log(`Connection database error \n${err}`);
});

const app = express();
const users = require("./routes/users");
const port = 3000;

// Middlewares
app.use(cors());
app.use(bodyParser.json());
app.use(passport.initialize());
app.use(passport.session());

require('./config/passport')(passport);

// Set Static Folder
app.use(express.static(path.join(__dirname, "public")));

app.use("/users", users);
// Index Route
app.get("/", (req, res, next) => {
  res.send("regarde moi ça");
});

// Start Server
app.listen(port, () => {
  console.log(`Server started on port ${port}`);
});
